package com.wiley.cache.utils;

import com.wiley.cache.model.CachePosition;
import org.jboss.marshalling.*;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.*;

/**
 * Created by Ivan Kornilov on 04.08.15.
 */
public class SerializationUtils {
    private static final MarshallerFactory marshallerFactory = Marshalling.getProvidedMarshallerFactory("river");
    private static final MarshallingConfiguration configuration = new MarshallingConfiguration();

    private static Unmarshaller unmarshaller;
    private static Marshaller marshaller;


    static {
        try {
            unmarshaller = marshallerFactory.createUnmarshaller(configuration);
            marshaller = marshallerFactory.createMarshaller(configuration);
        } catch (IOException e) {
            throw new RuntimeException();
        }
    }

    public static <T> void write(Path path, T object) {
        OutputStream os = null;
        try {
            os = Files.newOutputStream(path);
            marshaller.start(Marshalling.createByteOutput(os));
            marshaller.writeObject(object);
            marshaller.finish();

        } catch (IOException e) {
            System.err.print("Marshalling failed: ");
            e.printStackTrace();
        } finally {
            close(os);
        }
    }


    public static <T> T read(Path path) {
        if(!Files.exists(path) || !Files.isRegularFile(path)) {
           return null;
        }
        InputStream is = null;
        T result = null;
        try {
            is = Files.newInputStream(path);
            unmarshaller.start(Marshalling.createByteInput(is));
            result = (T)unmarshaller.readObject();
            unmarshaller.finish();
            is.close();

        } catch (IOException | ClassNotFoundException e) {
            System.err.print("Marshalling failed: ");
            e.printStackTrace();
        } finally {
            close(is);
        }
        return result;
    }

    public static <T> T read(ByteBuffer bytes) {
        T result;
        try {
            bytes.position(0);
            unmarshaller.start(Marshalling.createByteInput(bytes));
            result = (T)unmarshaller.readObject();
            unmarshaller.finish();
        } catch (IOException | ClassNotFoundException e) {
            System.err.print("Marshalling failed: ");
            throw new RuntimeException(e);
        }
        return result;
    }

    public static <T> ByteBuffer write(T object) {
        try {
            final ByteArrayOutputStream os = new ByteArrayOutputStream();

            marshaller.start(Marshalling.createByteOutput(os));
            marshaller.writeObject(object);
            marshaller.finish();
            return ByteBuffer.wrap(os.toByteArray());
        } catch (IOException e) {
            System.err.print("Marshalling failed: ");
            throw new RuntimeException(e);
        }
    }

    public static void close(Closeable closeable) {
        try {
            if(closeable != null) {
                closeable.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static <T> T readByCachePosition(Path path, CachePosition cachePosition) {
        FileChannel fileChannel = null;
        try {
            fileChannel = (FileChannel.open(path, StandardOpenOption.READ));
            fileChannel.position(cachePosition.getStart());
            ByteBuffer bytes = ByteBuffer.allocate(cachePosition.getBytes());
            fileChannel.read(bytes);
            return SerializationUtils.read(bytes);
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            SerializationUtils.close(fileChannel);
        }
    }

    public static <T> CachePosition writeValue(Path path, T value) {
        FileChannel fileChannel = null;
        try {
            CachePosition cachePosition = new CachePosition();
            fileChannel = (FileChannel.open(path, StandardOpenOption.WRITE));
            fileChannel.position(fileChannel.size());
            cachePosition.setStart(fileChannel.position());
            ByteBuffer bytes = SerializationUtils.write(value);

            fileChannel.write(bytes);
            cachePosition.setBytes( bytes.limit());

            return cachePosition;
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            SerializationUtils.close(fileChannel);
        }
    }

    public synchronized static void removeValue(Path path, CachePosition cachePosition) {
        FileChannel fileChannel = null;
        try {
            fileChannel = (FileChannel.open(path, StandardOpenOption.WRITE));
            fileChannel.position(cachePosition.getStart());
            fileChannel.truncate(cachePosition.getBytes());
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            SerializationUtils.close(fileChannel);
        }
    }


    public static void createFolderIfNotExists(String path) {
        Path folder = Paths.get(path);
        if(!Files.exists(folder, LinkOption.NOFOLLOW_LINKS)) {
            try {
                Files.createDirectory(folder);
            } catch (IOException e) {
                e.printStackTrace();
                throw new RuntimeException(e);
            }
        }
    }
}
