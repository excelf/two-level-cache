package com.wiley.cache;

import com.wiley.cache.actions.EvictionActionListener;
import com.wiley.cache.model.ICache;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.Map;

/**
 * Created by Ivan Kornilov on 01.08.15.
 */

@Component
public class TwoLevelCache<K, V> implements ICache<K, V> {
    @Autowired
    private MemoryCache<K, V> memoryCache;
    @Autowired
    private SingleFileCache<K, V> fileCache;

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @PostConstruct
    public void init() {
        memoryCache.setEvictionListener(new EvictionActionListener<K, V>() {
            @Override
            public void evictionAction(Map<K, V>evicted) {
                for (Map.Entry<K, V> entry : evicted.entrySet()) {
                    fileCache.put(entry.getKey(), entry.getValue());
                }
            }
        });
    }

    @PreDestroy
    public void preDestroy() {
        clear();
    }

    @Override
    public V get(K key) {
        V value = memoryCache.get(key);
        if(value != null) {
            return value;
        } else {
            return fileCache.get(key);
        }
    }

    @Override
    public void put(K key, V value) {
        memoryCache.put(key, value);
    }

    @Override
    public void setEvictionListener(EvictionActionListener<K, V> listener) {
        fileCache.setEvictionListener(listener);
    }

    @Override
    public int size() {
        logger.info("memoryCache size: " + memoryCache.size() + " fileCache size: " + fileCache.size());
        return memoryCache.size() + fileCache.size();
    }

    @Override
    public void clear() {
        memoryCache.clear();
        fileCache.clear();
    }

    @Override
    public V remove(K key) {
        V result = memoryCache.remove(key);
        if(result == null) {
            return fileCache.remove(key);
        }
        return result;
    }

}
