package com.wiley.cache;

import com.wiley.cache.actions.EvictionActionListener;
import com.wiley.cache.model.CachePosition;
import com.wiley.cache.model.ICache;
import com.wiley.cache.model.PropertyHolder;
import com.wiley.cache.utils.SerializationUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.File;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import static java.util.Map.Entry;

/**
 * Created by Ivan Kornilov on 01.08.15.
 */

/**
 *
 * Hold all items on single file on disk
 */

@Component
@Scope("prototype")
public class SingleFileCache<K, V> implements ICache<K, V> {

    protected static final String FILE_NAME = "cache";
    protected Path mainCachePath;
    protected Logger logger = LoggerFactory.getLogger(this.getClass());
    protected Lock lock = new ReentrantLock();

    @Autowired
    private MemoryCache<K, CachePosition> memoryCache;

    protected EvictionActionListener listener;
    protected List<K> keys;

    @Autowired
    private PropertyHolder propertyHolder;

    @PostConstruct
    public void init() {
        String tempFolder = System.getProperty("java.io.tmpdir") + "single-file-cache";

        logger.info("Temp Folder path: " + tempFolder);
        SerializationUtils.createFolderIfNotExists(tempFolder);

        memoryCache.setEvictionListener(new EvictionActionListener<K, CachePosition>() {
            @Override
            public void evictionAction(Map<K, CachePosition> evicted) {
                handleEviction(evicted);
            }
        });
        memoryCache.setMaxSize(propertyHolder.getMaxSizeFilesystem());
        mainCachePath = Paths.get(tempFolder + File.separator + FILE_NAME +  RandomStringUtils.randomAlphanumeric(10));
        logger.info("Temp File path: " + mainCachePath);
        createMainFile(mainCachePath);

        keys = new ArrayList<>();
    }

    private void createMainFile(Path path) {
        if(!Files.exists(path)) {
            try {
                Files.createFile(path);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        } else {
            clearFileContent(path);
        }
    }

    private void clearFileContent(Path path) {
        if(Files.exists(path)){
            try {
                FileChannel fileChannel = FileChannel.open(path, StandardOpenOption.WRITE, StandardOpenOption.TRUNCATE_EXISTING);
                fileChannel.close();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

    private void deleteFile(Path path) {
        try {
            Files.deleteIfExists(path);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void handleEviction(Map<K, CachePosition> evicted) {
        if(listener != null) {
            listener.evictionAction(evicted);
        }
        for(Entry<K, CachePosition> entry : evicted.entrySet()) {
            remove(entry.getKey());
        }
    }

    public V get(K key) {
        CachePosition result = memoryCache.get(key);
        lock.lock();
        try {
            if(result != null) {
                return SerializationUtils.readByCachePosition(mainCachePath, result);
            }
        } finally {
            lock.unlock();
        }
        return null;
    }

    public void put(K key, V value) {
        keys.add(key);
        lock.lock();
        try {
            CachePosition cachePosition = SerializationUtils.writeValue(mainCachePath, value);
            memoryCache.put(key, cachePosition);
        } finally {
            lock.unlock();
        }
    }

    @Override
    public void setEvictionListener(EvictionActionListener<K, V> listener) {
        this.listener = listener;
    }

    @Override
    public int size() {
        return memoryCache.size();
    }

    @Override
    public void clear() {
        memoryCache.clear();
        keys.clear();
        clearFileContent(mainCachePath);
    }

    @Override
    public V remove(K key) {
        CachePosition cachePosition = memoryCache.remove(key);
        V value = null;
        if(cachePosition != null) {
            lock.lock();
            try {
                value = SerializationUtils.readByCachePosition(mainCachePath, cachePosition);
                SerializationUtils.removeValue(mainCachePath, cachePosition);
                shiftCache(key, cachePosition);
            } finally {
                lock.unlock();
            }

        }

        return value;
    }

    private void shiftCache(K key, CachePosition cachePosition) {
        int position = keys.indexOf(key);
        for(int i = position; i < keys.size(); i++) {
            K currentKey = keys.get(i);
            CachePosition currentPosition = memoryCache.get(currentKey);
            currentPosition.shiftStart(cachePosition.getBytes());
        }
        keys.remove(position);
    }

    @PreDestroy
    public void preDestroy() {
        clear();
    }
}
