package com.wiley.cache;

import com.wiley.cache.actions.EvictionActionListener;
import com.wiley.cache.model.EvictionAlgorithm;
import com.wiley.cache.model.ICache;
import com.wiley.cache.model.ValueInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import static java.util.Map.Entry;

/**
 * Created by Ivan Kornilov on 03.08.2015.
 */
public abstract class ACache<K, V> implements ICache<K, V>  {

    protected static final double EVICTION_PERCENT = 0.2;
    protected int evictionQuantity;

    protected boolean saveAddedTime = false;//Сохранять ли время добавления в кэш

    protected ConcurrentMap<K, ValueInfo<V>> keyToInfo;

    protected EvictionActionListener listener;
    protected int maxSize;
    protected EvictionAlgorithm evictionAlgorithm;
    protected int lifetime;

    protected Logger logger = LoggerFactory.getLogger(this.getClass());

    protected ACache() {

    }

    protected void init(int maxSize, EvictionAlgorithm evictionAlgorithm, int lifetime) {
        this.maxSize = maxSize;
        this.evictionAlgorithm = evictionAlgorithm;
        this.lifetime = lifetime;

        evictionQuantity = (int)(maxSize * EVICTION_PERCENT);
        keyToInfo = new ConcurrentHashMap<>();
        saveAddedTime = lifetime != 0 ||
                EvictionAlgorithm.LRU.equals(evictionAlgorithm) ||
                EvictionAlgorithm.MRU.equals(evictionAlgorithm) ||
                EvictionAlgorithm.LFRU.equals(evictionAlgorithm);
    }

    protected void putInfo(K key, ValueInfo<V> valueInfo) {
        if(saveAddedTime) {
            valueInfo.setAddedTime(new Date());
        }
        keyToInfo.put(key, valueInfo);

        if(isNeedEviction(keyToInfo.size())) {
            evictItems(keyToInfo);//evict 20% of items
        }
    }

    @Override
    public void setEvictionListener(EvictionActionListener<K, V> listener) {
        this.listener = listener;
    }

    protected boolean isLifetimeOver(Date date) {
        return isLifetimeOver(date.getTime(), System.currentTimeMillis());
    }

    protected ValueInfo<V> getInfo(K key) {
        ValueInfo<V> valueInfo = keyToInfo.get(key);
        if(valueInfo != null) {
            if(EvictionAlgorithm.LFU.equals(evictionAlgorithm) || EvictionAlgorithm.LFRU.equals(evictionAlgorithm)) {
                valueInfo.getIncrementReceivedCount();
            }

            if(lifetime != 0 && isLifetimeOver(valueInfo.getAddedTime())) {
                remove(key);
                valueInfo = null;
            }
        }

        return valueInfo;
    }

    protected boolean isLifetimeOver(long time, long timeNow) {
        return (time + lifetime * 1000) < timeNow;
    }

    protected boolean isNeedEviction(int sizeNow) {
        return sizeNow > maxSize;
    }

    protected abstract void removeOldItems();

    protected void evictItems(Map<K, ValueInfo<V>> cacheMap) {
        Map<K, V> evictedMap = getEvictedMap(evictionQuantity, cacheMap);

        for(K key : evictedMap.keySet()) {
            remove(key);
        }

        if(listener != null){
            listener.evictionAction(evictedMap);
        }

        if(lifetime != 0) {
            removeOldItems();
        }
    }

    protected <K, V> Map<K, V> getEvictedMap(int quantity, Map<K, ValueInfo<V>> cacheMap) {

        List<Entry<K, ValueInfo<V>>> sortedList = new ArrayList<>(cacheMap.size());
        sortedList.addAll(cacheMap.entrySet());

        if(EvictionAlgorithm.LRU.equals(evictionAlgorithm)) {
            Collections.sort(sortedList, new Comparator<Entry<K, ValueInfo<V>>>() {
                @Override
                public int compare(Entry<K, ValueInfo<V>> o1, Entry<K, ValueInfo<V>> o2) {
                    return o1.getValue().getAddedTime().compareTo(o2.getValue().getAddedTime());
                }
            });
        } else if(EvictionAlgorithm.MRU.equals(evictionAlgorithm)) {
            Collections.sort(sortedList, new Comparator<Entry<K, ValueInfo<V>>>() {
                @Override
                public int compare(Entry<K, ValueInfo<V>> o1, Entry<K, ValueInfo<V>> o2) {
                    return o1.getValue().getAddedTime().compareTo(o2.getValue().getAddedTime()) * -1 ;
                }
            });
        } else if(EvictionAlgorithm.LFU.equals(evictionAlgorithm)) {
            Collections.sort(sortedList, new Comparator<Entry<K, ValueInfo<V>>>() {
                @Override
                public int compare(Entry<K, ValueInfo<V>> o1, Entry<K, ValueInfo<V>> o2) {
                    return Integer.compare(o1.getValue().getReceivedCount(), o2.getValue().getReceivedCount());
                }
            });
        } else if (EvictionAlgorithm.LFRU.equals(evictionAlgorithm))  {
            Collections.sort(sortedList, new Comparator<Entry<K, ValueInfo<V>>>() {
                @Override
                public int compare(Entry<K, ValueInfo<V>> o1, Entry<K, ValueInfo<V>> o2) {
                    int result = Integer.compare(o1.getValue().getReceivedCount(), o2.getValue().getReceivedCount());
                    if(result == 0) {
                        result = o1.getValue().getAddedTime().compareTo(o2.getValue().getAddedTime());
                    }
                    return result;
                }
            });
        } else {
            throw new IllegalArgumentException("Unsupported Eviction Algorithm");
        }

        Map<K, V> evictedMap = new HashMap<>();
        for(int i = 0; i < quantity; i++ ) {
            K key = sortedList.get(i).getKey();
            evictedMap.put(key, cacheMap.get(key).getValue());

        }

        return evictedMap;
    }


    @Override
    public int size() {
        return keyToInfo.size();
    }

    @Override
    public V remove(K key) {
        ValueInfo<V> valueInfo = keyToInfo.remove(key);
        return valueInfo != null ? valueInfo.getValue() : null;
    }


    public int getMaxSize() {
        return maxSize;
    }

    public void setMaxSize(int maxSize) {
        this.maxSize = maxSize;
    }

    public int getLifetime() {
        return lifetime;
    }

    public void setLifetime(int lifetime) {
        this.lifetime = lifetime;
    }

    public EvictionAlgorithm getEvictionAlgorithm() {
        return evictionAlgorithm;
    }

    public void setEvictionAlgorithm(EvictionAlgorithm evictionAlgorithm) {
        this.evictionAlgorithm = evictionAlgorithm;
    }
}

