package com.wiley.cache;

import com.wiley.cache.model.PropertyHolder;
import org.apache.log4j.Logger;
import org.infinispan.Cache;
import org.infinispan.configuration.cache.Configuration;
import org.infinispan.configuration.cache.ConfigurationBuilder;
import org.infinispan.configuration.global.GlobalConfiguration;
import org.infinispan.configuration.global.GlobalConfigurationBuilder;
import org.infinispan.eviction.EvictionStrategy;
import org.infinispan.manager.DefaultCacheManager;
import org.infinispan.manager.EmbeddedCacheManager;
import org.infinispan.notifications.Listener;
import org.infinispan.notifications.cachelistener.annotation.CacheEntriesEvicted;
import org.infinispan.notifications.cachelistener.event.CacheEntriesEvictedEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.concurrent.TimeUnit;

/**
 * Created by Ivan Kornilov on 22.07.15.
 */

@Component
@Scope("prototype")
public class InfinispanCache {
    private Cache<String, Object> memoryCache;
    private Cache<String, Object> fileCache;
    private int maxSizeFilesystem;
    private int maxSizeMemory;
    private Logger logger = Logger.getLogger(this.getClass());
    private EvictionStrategy evictionStrategy;

    @Autowired
    protected PropertyHolder propertyHolder;

    @PostConstruct
    public void init() {
        evictionStrategy = EvictionStrategy.LRU;
        maxSizeMemory = propertyHolder.getMaxSizeMemory();
        maxSizeFilesystem  = propertyHolder.getMaxSizeFilesystem();

        GlobalConfiguration globalConfiguration = new GlobalConfigurationBuilder()
                .globalJmxStatistics().allowDuplicateDomains(true).build();

        Configuration configurationFilesystem = new ConfigurationBuilder().persistence()
                .addSingleFileStore()
                .location(System.getProperty("java.io.tmpdir"))
                .maxEntries(maxSizeFilesystem)
                .eviction()
                .strategy(evictionStrategy)
                .maxEntries(maxSizeFilesystem)
                .expiration()
                .lifespan(propertyHolder.getLifetime(), TimeUnit.SECONDS)
                .build();

        EmbeddedCacheManager fileManager = new DefaultCacheManager(globalConfiguration, configurationFilesystem);
        fileCache = fileManager.getCache("filesystem-cache");


        Configuration configurationMemory = new ConfigurationBuilder()
               .eviction()
               .strategy(evictionStrategy)
               .maxEntries(maxSizeMemory)
               .expiration()
               .lifespan(propertyHolder.getLifetime(), TimeUnit.SECONDS)
               .build();

        EmbeddedCacheManager memoryManager = new DefaultCacheManager(globalConfiguration, configurationMemory);

        memoryCache = memoryManager.getCache("memory-cache");
        memoryCache.addListener(new EvictedEventListener(fileCache));
    }

    public void processEviction() {
        memoryCache.getAdvancedCache().getEvictionManager().processEviction();
        fileCache.getAdvancedCache().getEvictionManager().processEviction();
    }


    public void put(String key, Object value) {
        memoryCache.put(key, value);
    }

    public Object get(String key) {
        if(memoryCache.containsKey(key)) {
            return memoryCache.get(key);
        } else {
            return fileCache.get(key);
        }
    }

    public int getSize() {
        logger.info("memoryCache " + memoryCache.size());
        logger.info("fileCache  " + fileCache.size());
        return memoryCache.size() + fileCache.size();
    }

    @Listener
    public class EvictedEventListener{
        private Cache cacheForPush;

        EvictedEventListener(Cache cacheForPush) {
            this.cacheForPush = cacheForPush;
        }

        @CacheEntriesEvicted
        public void putToFileCache(CacheEntriesEvictedEvent event) {
            cacheForPush.putAll(event.getEntries());
        }
    }

    public int getMaxSizeFilesystem() {
        return maxSizeFilesystem;
    }

    public int getMaxSizeMemory() {
        return maxSizeMemory;
    }

    public void stop() {
        memoryCache.stop();
        fileCache.stop();
    }

}
