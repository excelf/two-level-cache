package com.wiley.cache.actions;

import java.util.Map;

/**
 * Created by excelf on 01.08.15.
 */
public interface EvictionActionListener<K, V> {

    public void evictionAction(Map<K, V> evicted);
}
