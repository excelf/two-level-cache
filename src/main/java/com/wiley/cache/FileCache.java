package com.wiley.cache;

import com.wiley.cache.actions.EvictionActionListener;
import com.wiley.cache.model.ICache;
import com.wiley.cache.model.PropertyHolder;
import com.wiley.cache.utils.SerializationUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;

import static java.util.Map.Entry;


/**
 * Created by Ivan Kornilov on 01.08.15.
 *
 * Create different file for every item, on disk. Not best solution, it can slow down the operating system on a large number of items.
 * @see SingleFileCache
 */
@Component
@Scope("prototype")
public class FileCache<K, V> implements ICache<K, V> {

    protected static final String FOLDER_PATH = "file-cache";
    protected String tempFolder;
    protected Path tempFolderFile;
    protected EvictionActionListener listener;
    protected Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private PropertyHolder propertyHolder;

    @Autowired
    private MemoryCache<K, String> memoryCache;


    @PostConstruct
    public void init() {
        memoryCache.setEvictionListener(new EvictionActionListener<K, String>() {
            @Override
            public void evictionAction(Map<K, String> evicted) {
                handleEviction(evicted);
            }
        });
        memoryCache.setMaxSize(propertyHolder.getMaxSizeFilesystem());
        tempFolder = System.getProperty("java.io.tmpdir") + FOLDER_PATH;

        logger.info("Temp Folder path: " + tempFolder);


        tempFolderFile = Paths.get(tempFolder);
        SerializationUtils.createFolderIfNotExists(tempFolder);
    }

    @Override
    public V get(K key) {
        String fileName = memoryCache.get(key);
        if(fileName != null) {
           return SerializationUtils.read(fileFromKey(fileName));
        }
        return null;
    }

    private void handleEviction(Map<K, String> evicted) {
        if(listener != null) {
            listener.evictionAction(evicted);
        }
        for(Entry<K, String> entry : evicted.entrySet()) {
            remove(entry.getKey());
        }
    }

    @Override
    public void put(K key, V value) {
        String fileName = newFileNameFromKey(key, value);

        SerializationUtils.write(fileFromKey(fileName), value);
    }

    @Override
    public void setEvictionListener(EvictionActionListener<K, V> listener) {
        this.listener = listener;
    }

    @Override
    public int size() {
        return memoryCache.size();
    }

    public String newFileNameFromKey(K key, V value) {;
        String oldFileName = memoryCache.get(key);
        String newFileName;

        do {
            newFileName = RandomStringUtils.randomAlphanumeric(10) + key.hashCode() + value.hashCode();
        } while (newFileName.equals(oldFileName));

        memoryCache.put(key, newFileName);
        return newFileName;

    }


    private void removeFile(String fileName) {
        if(fileName == null) {
            return;
        }
        try {
            Files.deleteIfExists(fileFromKey(fileName));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void clear() {
        memoryCache.clear();
        try {
            for(Path path : Files.newDirectoryStream(tempFolderFile)) {
                Files.deleteIfExists(path);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public V remove(K key) {
        V value = get(key);
        String fileName = memoryCache.get(key);
        if(fileName != null) {
            removeFile(fileName);
        }
        return value;
    }

    private Path fileFromKey(String fileName) {
        return Paths.get(tempFolder + File.separator + fileName);
    }

    @PreDestroy
    public void preDestroy() {
        clear();
    }
}
