package com.wiley.cache;

import com.wiley.cache.model.PropertyHolder;
import com.wiley.cache.model.ValueInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.Iterator;

import static java.util.Map.Entry;

/**
 * Created by Ivan Kornilov on 01.08.15.
 */

@Component
@Scope("prototype")
public class MemoryCache<K, V> extends ACache<K, V> {

    @Autowired
    private PropertyHolder propertyHolder;

    @PostConstruct
    public void init() {
        init(propertyHolder.getMaxSizeMemory(), propertyHolder.getEvictionStrategy(), propertyHolder.getLifetime());

    }

    @Override
     public V get(K key) {
        ValueInfo<V> valueInfo = getInfo(key);
        return valueInfo == null ? null : valueInfo.getValue();
    }

    @Override
    public void put(K key, V value) {
        putInfo(key, new ValueInfo<>(value));
    }

    protected void removeOldItems() {
        long timeNow = System.currentTimeMillis();

        for(Iterator<Entry<K, ValueInfo<V>>> iterator = keyToInfo.entrySet().iterator(); iterator.hasNext();) {
            Entry<K, ValueInfo<V>> entry = iterator.next();
            if(isLifetimeOver(entry.getValue().getAddedTime().getTime(), timeNow)) {
                iterator.remove();
            }
        }
    }

    @PreDestroy
    public void preDestroy() {
        clear();
    }

    @Override
    public void clear() {
        keyToInfo.clear();
    }
}
