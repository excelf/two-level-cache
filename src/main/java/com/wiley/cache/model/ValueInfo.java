package com.wiley.cache.model;

import java.util.Date;

/**
 * Created by Ivan Kornilov on 01.08.15.
 */
public class ValueInfo<T> {

    private T value;
    private int receivedCount = 0;
    private Date addedTime;

    public ValueInfo() {

    }

    public ValueInfo(T value) {
        this.value = value;
    }

    public int getReceivedCount() {
        return receivedCount;
    }

    public void setReceivedCount(int receivedCount) {
        this.receivedCount = receivedCount;
    }

    public void getIncrementReceivedCount() {
        receivedCount++;
    }

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }

    public Date getAddedTime() {
        return addedTime;
    }

    public void setAddedTime(Date addedTime) {
        this.addedTime = addedTime;
    }
}
