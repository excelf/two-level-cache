package com.wiley.cache.model;

/**
 * Created by excelf on 08.08.15.
 */
public class CachePosition {

    long start;
    int bytes;

    public int getBytes() {
        return bytes;
    }

    public void setBytes(int bytes) {
        this.bytes = bytes;
    }

    public long getStart() {
        return start;
    }

    public void setStart(long start) {
        this.start = start;
    }

    public void shiftStart(long bytes) {
        start = start - bytes;
        if(this.start < 0) {
            throw new RuntimeException("CachePosition start < 0 " + start);
        }
    }
}
