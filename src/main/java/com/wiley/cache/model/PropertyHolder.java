package com.wiley.cache.model;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Created by Ivan Kornilov  on 04.08.15.
 */

@Component
public class PropertyHolder {
    @Value("${max-size-memory}")
    int maxSizeMemory;

    @Value("${max-size-filesystem}")
    int maxSizeFilesystem;

    @Value("${eviction-strategy}")
    EvictionAlgorithm evictionStrategy;

    @Value("${lifetime}")
    int lifetime;

    public int getMaxSizeFilesystem() {
        return maxSizeFilesystem;
    }

    public void setMaxSizeFilesystem(int maxSizeFilesystem) {
        this.maxSizeFilesystem = maxSizeFilesystem;
    }

    public EvictionAlgorithm getEvictionStrategy() {
        return evictionStrategy;
    }

    public void setEvictionStrategy(EvictionAlgorithm evictionStrategy) {
        this.evictionStrategy = evictionStrategy;
    }

    public int getLifetime() {
        return lifetime;
    }

    public void setLifetime(int lifetime) {
        this.lifetime = lifetime;
    }

    public int getMaxSizeMemory() {
        return maxSizeMemory;
    }

    public void setMaxSizeMemory(int maxSizeMemory) {
        this.maxSizeMemory = maxSizeMemory;
    }
}
