package com.wiley.cache.model;

import com.wiley.cache.actions.EvictionActionListener;

/**
 * Created by Ivan Kornilov  on 01.08.15.
 */
public interface ICache<K, V> {

    public V get(K key);

    public void put(K key, V value);

    public void setEvictionListener(EvictionActionListener<K, V> listener);

    public int size();

    public void clear();

    public V remove(K key);
}
