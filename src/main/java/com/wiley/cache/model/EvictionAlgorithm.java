package com.wiley.cache.model;

/**
 * Created by Ivan Kornilov  on 02.08.15.
 */
public enum  EvictionAlgorithm {
    LFU,
    LRU,
    MRU,
    LFRU //LFRU use combination of LFU LRU algorithms
}
