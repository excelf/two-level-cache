package com.wiley;


import com.wiley.cache.FileCache;
import com.wiley.cache.SingleFileCache;
import com.wiley.cache.TwoLevelCache;
import com.wiley.cache.actions.EvictionActionListener;
import com.wiley.cache.model.PropertyHolder;
import com.wiley.model.Book;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Map;

/**
 * Created by Ivan Kornilov on 22.07.15.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:spring-config.xml")
public class SpringTest extends Assert {

    @Autowired
    private TwoLevelCache<String, Book> twoLevelCache;

    @Autowired
    private FileCache<String, Book> fileCache;

    @Autowired
    private PropertyHolder propertyHolder;

    static Logger logger = LoggerFactory.getLogger(SpringTest.class);

    @Test
    public void testTwoLevelCacheLFU() throws InterruptedException {
        Book firstBook = getRandomBook();
        firstBook.setId("firstBookId");
        twoLevelCache.put(firstBook.getId(), firstBook);

        Book firstBookFromCache = twoLevelCache.get(firstBook.getId());
        assertNotNull(firstBookFromCache);//get object from cache
        logger.info("firstBookId=" + firstBookFromCache.getId());
        assert firstBook.getId().equals(firstBookFromCache.getId());

        twoLevelCache.setEvictionListener(new EvictionActionListener<String, Book>() {
            @Override
            public void evictionAction(Map<String, Book> evicted) {
                logger.info("evicted.size: " + evicted.size());
                for(Map.Entry<String, Book> entry : evicted.entrySet()) {
                    logger.info("evicted item: " + entry.getKey());
                }
            }
        });

        for(int i = 0;i < 200; i++) {
            Book book = getRandomBook();
            twoLevelCache.put(book.getId(), book);
        }

        Book firstBook2 = twoLevelCache.get(firstBook.getId());
        assertNotNull(firstBook2);//If LFU does not work this item must be evicted
        logger.info("firstBookId2=" + firstBook2.getId());


        for(int i = 0;i < 200; i++) {
            Book book = getRandomBook();
            twoLevelCache.put(book.getId(), book);
        }

        Book firstBook3 = twoLevelCache.get(firstBook.getId());
        assertNotNull(firstBook3);//If LFU does not work this item must be evicted{
        logger.info("firstBookId3=" + firstBook3.getId());



        int expectedSize = (int)((propertyHolder.getMaxSizeFilesystem() + propertyHolder.getMaxSizeMemory()));
        int sizeNow = twoLevelCache.size();
        assertTrue("Min expected " + expectedSize *0.8 + " Max expected " + expectedSize * 1.2 + " but was " + sizeNow,
                0.8 < sizeNow && sizeNow < expectedSize * 1.2);
        logger.info("Cache size=" + twoLevelCache.size());
    }

    @Test
    public void testFileCache() throws InterruptedException {
        Book firstBook = getRandomBook();
        fileCache.put(firstBook.getId(), firstBook);
        logger.info("firstBook id=" + firstBook.getId());

        Book firstBookFromCache = fileCache.get(firstBook.getId());
        assertNotNull(firstBookFromCache);//get object from cache
        logger.info(firstBookFromCache.getId());


        assert firstBook.getId().equals(firstBookFromCache.getId());

        for(int i = 0;i < 200; i++) {
            Book book = getRandomBook();
            fileCache.put(book.getId(), book);
        }
    }


    @Autowired
    SingleFileCache<String, Book> singleFileCache;

    @Test
    public void testSingleFileCache() throws InterruptedException {
        Book firstBook = getRandomBook();
        firstBook.setId("firstBook");
        singleFileCache.put(firstBook.getId(), firstBook);
        logger.info("firstBook id=" + firstBook.getId());

        singleFileCache.setEvictionListener(new EvictionActionListener<String, Book>() {
            @Override
            public void evictionAction(Map<String, Book> evicted) {
                logger.info("evicted.size: " + evicted.size());
                for(Map.Entry<String, Book> entry : evicted.entrySet()) {
                    logger.info("evicted item: " + entry.getKey());
                }
            }
        });

        Book firstBookFromCache = singleFileCache.get(firstBook.getId());
        assertNotNull(firstBookFromCache);//get object from cache
        logger.info("firstBook id=" + firstBookFromCache.getId());


        assertEquals(firstBook.getId(), firstBookFromCache.getId());

        for(int i = 0;i < 200; i++) {
            Book book = getRandomBook();
            singleFileCache.put(book.getId(), book);
        }

        firstBookFromCache = singleFileCache.get(firstBook.getId());
        assertNotNull(firstBookFromCache);//get object from cache
        logger.info("firstBook id=" + firstBookFromCache.getId());

        Book secondBook = getRandomBook();
        secondBook.setId("secondBook");
        singleFileCache.put(secondBook.getId(), secondBook);
        logger.info("secondBook id=" + secondBook.getId());

        Book secondBookFromCache = singleFileCache.get(secondBook.getId());
        assertNotNull(secondBookFromCache);//get object from cache
        logger.info("secondBookFromCache id=" + secondBookFromCache.getId());

        assertEquals(secondBook.getId(), secondBookFromCache.getId());
    }

    public Book getRandomBook() {
        Book book = new Book();
        //fill fields by random content
        book.setId(RandomStringUtils.random(5));
        book.setAuthor(RandomStringUtils.random(10));
        book.setDescription(RandomStringUtils.random(20));
        book.setTitle(RandomStringUtils.random(10));
        return book;
    }
}
